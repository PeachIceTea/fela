module github.com/PeachIceTea/fela

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
	github.com/gin-gonic/gin v1.6.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/hashicorp/go-envparse v0.0.0-20200406174449-d9cfd743a15e
	github.com/jmoiron/sqlx v1.2.0
	golang.org/x/crypto v0.0.0-20200420201142-3c4aac89819a
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a
)
